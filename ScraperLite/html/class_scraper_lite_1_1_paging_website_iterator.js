var class_scraper_lite_1_1_paging_website_iterator =
[
    [ "__construct", "class_scraper_lite_1_1_paging_website_iterator.html#ae8b7d1548323d49e0d2fdf1095624cf8", null ],
    [ "current", "class_scraper_lite_1_1_paging_website_iterator.html#af343507d1926e6ecf964625d41db528c", null ],
    [ "key", "class_scraper_lite_1_1_paging_website_iterator.html#a729e946b4ef600e71740113c6d4332c0", null ],
    [ "next", "class_scraper_lite_1_1_paging_website_iterator.html#acea62048bfee7b3cd80ed446c86fb78a", null ],
    [ "rewind", "class_scraper_lite_1_1_paging_website_iterator.html#ae619dcf2218c21549cb65d875bbc6c9c", null ],
    [ "valid", "class_scraper_lite_1_1_paging_website_iterator.html#abb9f0d6adf1eb9b3b55712056861a247", null ],
    [ "$pageOffset", "class_scraper_lite_1_1_paging_website_iterator.html#a53526464fc6c5edfdb64db50433e94ff", null ],
    [ "$pageUrl", "class_scraper_lite_1_1_paging_website_iterator.html#ae3abf65b0d870c726d29d7d455d1d363", null ],
    [ "$pagingWebsite", "class_scraper_lite_1_1_paging_website_iterator.html#ad50adc155b09e3c8894d2fbd5c0d2ed3", null ]
];