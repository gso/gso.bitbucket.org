var hierarchy =
[
    [ "AbstractNodeData", "class_scraper_lite_1_1_abstract_node_data.html", [
      [ "AbstractNodeDataItem", "class_scraper_lite_1_1_abstract_node_data_item.html", [
        [ "AbstractDOMNodeData", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html", [
          [ "AttributeValue", "class_scraper_lite_1_1_attribute_value.html", null ],
          [ "DOMNodeData", "class_scraper_lite_1_1_d_o_m_node_data.html", null ],
          [ "ElementOuterXML", "class_scraper_lite_1_1_element_outer_x_m_l.html", null ],
          [ "ElementText", "class_scraper_lite_1_1_element_text.html", null ]
        ] ],
        [ "DOMNode", "class_scraper_lite_1_1_d_o_m_node.html", null ]
      ] ],
      [ "AbstractNodeDataList", "class_scraper_lite_1_1_abstract_node_data_list.html", [
        [ "AbstractDOMNodeListData", "class_scraper_lite_1_1_abstract_d_o_m_node_list_data.html", [
          [ "AbstractList", "class_scraper_lite_1_1_abstract_list.html", [
            [ "HTMLList", "class_scraper_lite_1_1_h_t_m_l_list.html", null ],
            [ "ListData", "class_scraper_lite_1_1_list_data.html", null ]
          ] ],
          [ "AbstractTable", "class_scraper_lite_1_1_abstract_table.html", [
            [ "DataRecord", "class_scraper_lite_1_1_data_record.html", null ],
            [ "HTMLTable", "class_scraper_lite_1_1_h_t_m_l_table.html", null ],
            [ "TableData", "class_scraper_lite_1_1_table_data.html", null ]
          ] ],
          [ "DOMDocumentData", "class_scraper_lite_1_1_d_o_m_document_data.html", null ],
          [ "DOMNodeListData", "class_scraper_lite_1_1_d_o_m_node_list_data.html", null ]
        ] ],
        [ "PageData", "class_scraper_lite_1_1_page_data.html", null ]
      ] ]
    ] ],
    [ "AbstractWebDocument", "class_scraper_lite_1_1_abstract_web_document.html", [
      [ "PagedList", "class_scraper_lite_1_1_paged_list.html", null ],
      [ "PagingWebsite", "class_scraper_lite_1_1_paging_website.html", null ],
      [ "WebPage", "class_scraper_lite_1_1_web_page.html", null ],
      [ "XMLDocument", "class_scraper_lite_1_1_x_m_l_document.html", null ]
    ] ],
    [ "Exception", null, [
      [ "CurlFetchException", "class_scraper_lite_1_1_curl_fetch_exception.html", null ],
      [ "DataNodeException", "class_scraper_lite_1_1_data_node_exception.html", null ],
      [ "DOMXPathQueryException", "class_scraper_lite_1_1_d_o_m_x_path_query_exception.html", null ],
      [ "WebDocumentException", "class_scraper_lite_1_1_web_document_exception.html", null ]
    ] ],
    [ "Iterator", null, [
      [ "AbstractDOMNodeListDataIterator", "class_scraper_lite_1_1_abstract_d_o_m_node_list_data_iterator.html", null ],
      [ "DataRecordIterator", "class_scraper_lite_1_1_data_record_iterator.html", null ],
      [ "PagedListIterator", "class_scraper_lite_1_1_paged_list_iterator.html", null ],
      [ "PagingWebsiteIterator", "class_scraper_lite_1_1_paging_website_iterator.html", null ]
    ] ],
    [ "IteratorAggregate", null, [
      [ "AbstractNodeDataList", "class_scraper_lite_1_1_abstract_node_data_list.html", null ],
      [ "PagedList", "class_scraper_lite_1_1_paged_list.html", null ],
      [ "PagingWebsite", "class_scraper_lite_1_1_paging_website.html", null ]
    ] ]
];