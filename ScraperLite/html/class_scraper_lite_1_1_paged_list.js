var class_scraper_lite_1_1_paged_list =
[
    [ "__construct", "class_scraper_lite_1_1_paged_list.html#a5f7fd4763199c18042eafb8c51cd4117", null ],
    [ "getIterator", "class_scraper_lite_1_1_paged_list.html#adb51ff85d44996e22fc98b9cb36f2ab8", null ],
    [ "loadDomDocument", "class_scraper_lite_1_1_paged_list.html#a61b7ff3eeef0fa8f4d9094020d4ce038", null ],
    [ "loadPageAtOffset", "class_scraper_lite_1_1_paged_list.html#af3ae82754d420e339c48cb50121d5c77", null ],
    [ "pageUrlAtOffset", "class_scraper_lite_1_1_paged_list.html#a0ae75100a91e44d947e47764a37b2c5a", null ],
    [ "$listIteratorCallback", "class_scraper_lite_1_1_paged_list.html#a0e562fe8a4b359046c4f472c1a1ee4b7", null ],
    [ "$pageUrlCallback", "class_scraper_lite_1_1_paged_list.html#a33b1b8b772998014ff219f8dd6a14f3b", null ]
];