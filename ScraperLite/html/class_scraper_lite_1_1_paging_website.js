var class_scraper_lite_1_1_paging_website =
[
    [ "__construct", "class_scraper_lite_1_1_paging_website.html#a020d930929804d1e73cbceceeda3da89", null ],
    [ "getIterator", "class_scraper_lite_1_1_paging_website.html#adb51ff85d44996e22fc98b9cb36f2ab8", null ],
    [ "loadDomDocument", "class_scraper_lite_1_1_paging_website.html#a61b7ff3eeef0fa8f4d9094020d4ce038", null ],
    [ "loadPageAtOffset", "class_scraper_lite_1_1_paging_website.html#af3ae82754d420e339c48cb50121d5c77", null ],
    [ "pageUrlAtOffset", "class_scraper_lite_1_1_paging_website.html#a0ae75100a91e44d947e47764a37b2c5a", null ],
    [ "pageUrlCallback", "class_scraper_lite_1_1_paging_website.html#ad5a37ac6abe9707f7c7ddba51ceb07a1", null ],
    [ "setPageUrlCallback", "class_scraper_lite_1_1_paging_website.html#a39c178172ca7542825b0784d0e0ad38b", null ]
];