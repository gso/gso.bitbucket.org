var class_scraper_lite_1_1_abstract_web_document =
[
    [ "__construct", "class_scraper_lite_1_1_abstract_web_document.html#af5d920c35dfc920ba9dec513621f6ec7", null ],
    [ "curlOptions", "class_scraper_lite_1_1_abstract_web_document.html#a8d18cad12f0d5bc361713c3126e602c9", null ],
    [ "domDocument", "class_scraper_lite_1_1_abstract_web_document.html#a6c2d71033c757115285351887e6009b7", null ],
    [ "domXPath", "class_scraper_lite_1_1_abstract_web_document.html#ac8f6e5271b1e0a722fc3d5964cfdd550", null ],
    [ "libxmlErrors", "class_scraper_lite_1_1_abstract_web_document.html#ac0c7b85300625f4eee74e9ccdf9d1746", null ],
    [ "load", "class_scraper_lite_1_1_abstract_web_document.html#a18b88cc71d0b77c7de2b759a07005c85", null ],
    [ "loadDomDocument", "class_scraper_lite_1_1_abstract_web_document.html#a61b7ff3eeef0fa8f4d9094020d4ce038", null ],
    [ "refresh", "class_scraper_lite_1_1_abstract_web_document.html#a2aff0844db6d9b56e6ba96123e1dd11c", null ],
    [ "setCurlOptions", "class_scraper_lite_1_1_abstract_web_document.html#a13a035c42c5e39d0c67cb6563a39922d", null ],
    [ "setUrl", "class_scraper_lite_1_1_abstract_web_document.html#a0b6d0c531aa70b1811b166299edab8d0", null ],
    [ "source", "class_scraper_lite_1_1_abstract_web_document.html#a6cfde826a3d3092bd8a3a636e2336bbb", null ],
    [ "url", "class_scraper_lite_1_1_abstract_web_document.html#abc5ed2ad3f9365fb31cc1eb625b974d9", null ],
    [ "$url", "class_scraper_lite_1_1_abstract_web_document.html#acf215f34a917d014776ce684a9ee8909", null ]
];