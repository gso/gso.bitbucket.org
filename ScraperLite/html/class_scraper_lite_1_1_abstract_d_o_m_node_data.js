var class_scraper_lite_1_1_abstract_d_o_m_node_data =
[
    [ "__construct", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#abf5112e7d814d91bf856300b2c6e2e76", null ],
    [ "dataXPathQuery", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#aa3b171892a8391954dc6823269e2972e", null ],
    [ "domNode", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#a68aa64d464445d6afd60efe3d0be1fd9", null ],
    [ "queryContext", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#a4a0b0eea7e95aa948b5d9531797bc9cb", null ],
    [ "queryResult", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#ad7a6f83e7c5f9ff827fb5007772ab412", null ],
    [ "setDataXPathQuery", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#a8ad4294a33c8cbd580d507b58856b550", null ],
    [ "setQueryContext", "class_scraper_lite_1_1_abstract_d_o_m_node_data.html#a0a0adfe83319ac4bb27e54095f1a3b12", null ]
];